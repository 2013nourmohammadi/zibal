# Generated by Django 3.2.6 on 2021-10-08 11:26

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('amount', models.IntegerField(blank=True, null=True)),
                ('createdAt', models.DateTimeField(blank=True, null=True)),
                ('merchantId', models.CharField(max_length=30)),
            ],
        ),
    ]
