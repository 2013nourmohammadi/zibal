from django.db import models


class Transaction(models.Model):
    amount = models.IntegerField(blank=True, null=True)
    createdAt = models.DateTimeField(blank=True, null=True)
    merchantId = models.CharField(max_length=30)
